#ifndef MAIN_SW_H
#define MAIN_SW_H

#include "player_sw.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/*
    CONSTS
*/

#define MAIN_SW_DEBUG     1


#define RETURN_NOTHING     1
#define ERROR_INVALID_ARGS 2
#define ERROR_INVALID_PORT 3
#define ERROR_INVALID_HOST 4
#define ERROR_SW           5

#define RETURN_SUCCESS     0
#define PLAY_OK            0

#define ARGS_MAX           4

#define ARGS_HELP        "-h"  // Return a help text
#define ARGS_HOST_NUMBER "-s"  // Host addr used
#define ARGS_PORT_NUMBER "-p"  // Host port number used

/*
    Text ERRORS
*/
#define ERR_HOST_NUMBER   "Endereço do servidor inválido!"
#define ERR_PORT_NUMBER   "Numero da porta do servidor invalido!"
#define ERR_PORT_RESERVED "Numero da porta escolhida eh reservado!"

void help();

#endif
