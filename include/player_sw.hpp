#ifndef PLAYER_SW_H
#define PLAYER_SW_H

#include "environm.h"
#include "types_fuzzy.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PLAYER_SW_DEBUG          1

#define PLAYER_SW_ERROR_CONN     1

class PlayerSW {

    public:

        PlayerSW();
        ~PlayerSW();

        void SetHost(char* host, int port);
        int  Play();

    private:

        char*   host_ip;
        int     host_port;

        float   ballAngle;
        float   targetAngle;
        float   ballDist;

        float   leftMotor;
        float   rightMotor;

        TFuzzy  fuzzy;

        // Declaração do objeto que representa o ambiente.
        environm::soccer::clientEnvironm environment;


};

#endif // PLAYER_SW_H
