#ifndef TYPES_FUZZY_H
#define TYPES_FUZZY_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <algorithm>
#include <list>

#define MAX_LENGTH_TYPES           3

#define MAX_LENGTH_ROLES           9

#define TYPES_FUZZY_DEBUG          1

enum typeFuzzySet { ESQUERDA = 0, FRENTE = 1, DIREITA = 2 };

typedef struct fuzzySet_t{

    float        alfa;
    float        beta;
    float        gama;

}fuzzySet;

class TFuzzy {

    public:

        TFuzzy();
        ~TFuzzy();

        void SetConjAlvo(typeFuzzySet set, float alfa, float beta, float gama);
        void SetConjBola(typeFuzzySet set, float alfa, float beta, float gama);
        void SetConjRobot(typeFuzzySet set, float alfa, float beta, float gama);

        void SetRegra1(typeFuzzySet alvo, typeFuzzySet bola, typeFuzzySet value);

        float CalculaRegrasR(float xAlvo, float xBola );

        float z;
        typeFuzzySet direction;
        float f_value[MAX_LENGTH_TYPES];

    private:

        fuzzySet alvo[MAX_LENGTH_TYPES];
        fuzzySet bola[MAX_LENGTH_TYPES];

        fuzzySet robot[MAX_LENGTH_TYPES];

        typeFuzzySet regras_E_alvoBola[MAX_LENGTH_TYPES][MAX_LENGTH_TYPES];


        float f_alvo[MAX_LENGTH_TYPES];
        float f_bola[MAX_LENGTH_TYPES];

        float f_regras_E_alvoBola[MAX_LENGTH_TYPES][MAX_LENGTH_TYPES];

        float robotAlturas[MAX_LENGTH_TYPES];

        float calcPertAlvo(int set, float x);
        float calcPertBola(int set, float x);
        float calcPertRobot(int set, float x);

        float calcTriagulo(float x, float alfa,float beta, float gama);

};

#endif // TYPES_FUZZY_H

