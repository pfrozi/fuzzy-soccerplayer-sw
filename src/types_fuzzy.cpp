#include "../include/types_fuzzy.hpp"

TFuzzy::TFuzzy(){

}
TFuzzy::~TFuzzy(){

}
void TFuzzy::SetConjAlvo(typeFuzzySet set, float alfa, float beta, float gama){

    alvo[set].alfa = alfa;
    alvo[set].beta = beta;
    alvo[set].gama = gama;

}
void TFuzzy::SetConjBola(typeFuzzySet set, float alfa, float beta, float gama){

    bola[set].alfa = alfa;
    bola[set].beta = beta;
    bola[set].gama = gama;

}
void TFuzzy::SetConjRobot(typeFuzzySet set, float alfa, float beta, float gama){

    robot[set].alfa = alfa;
    robot[set].beta = beta;
    robot[set].gama = gama;

}
void TFuzzy::SetRegra1(typeFuzzySet alvo, typeFuzzySet bola, typeFuzzySet value){

    regras_E_alvoBola[alvo][bola] = value;
}
float TFuzzy::calcPertAlvo(int set, float x){

    return calcTriagulo(x
                       ,alvo[set].alfa
                       ,alvo[set].beta
                       ,alvo[set].gama);
}

float TFuzzy::calcPertBola(int set, float x){

    return calcTriagulo(x
                       ,bola[set].alfa
                       ,bola[set].beta
                       ,bola[set].gama);
}
float TFuzzy::calcPertRobot(int set, float x){

    return calcTriagulo(x
                       ,robot[set].alfa
                       ,robot[set].beta
                       ,robot[set].gama);
}

float TFuzzy::calcTriagulo(float x, float alfa,float beta, float gama){

    if((x < alfa) || (x > gama)) return 0;
    if((x >= alfa) && (x <= beta)) return (x - alfa)/(beta - alfa);
    if((x > beta)  && (x <= gama)) return (gama - x)/(gama - beta);

    return 0.0f;
}

float TFuzzy::CalculaRegrasR(float xAlvo, float xBola){

    float ret = 0.0f;

    for(int k=0;k<MAX_LENGTH_TYPES;k++){

        f_alvo[k] = calcPertAlvo(k, xAlvo);
        f_bola[k] = calcPertBola(k, xBola);

        robotAlturas[k] = FLT_MIN;
    }


    for(int i=0;i<MAX_LENGTH_TYPES;i++){

        for(int j=0;j<MAX_LENGTH_TYPES;j++){

            // calcula a forca de disparo das regras
            f_regras_E_alvoBola[i][j] = std::min(f_alvo[i], f_bola[j]);

        }
    }

    // calcula as alturas
    for(int i=0;i<MAX_LENGTH_TYPES;i++){

        for(int j=0;j<MAX_LENGTH_TYPES;j++){

            if(regras_E_alvoBola[i][j]==typeFuzzySet::ESQUERDA){

                robotAlturas[typeFuzzySet::ESQUERDA]
                  = std::max(robotAlturas[typeFuzzySet::ESQUERDA], f_regras_E_alvoBola[i][j]);

            }if(regras_E_alvoBola[i][j]==typeFuzzySet::FRENTE){

                robotAlturas[typeFuzzySet::FRENTE]
                  = std::max(robotAlturas[typeFuzzySet::FRENTE], f_regras_E_alvoBola[i][j]);

            }if(regras_E_alvoBola[i][j]==typeFuzzySet::DIREITA){

                robotAlturas[typeFuzzySet::DIREITA]
                  = std::max(robotAlturas[typeFuzzySet::DIREITA], f_regras_E_alvoBola[i][j]);

            }

        }
    }

    float div = (robotAlturas[typeFuzzySet::ESQUERDA] + robotAlturas[typeFuzzySet::FRENTE] + robotAlturas[typeFuzzySet::DIREITA]);

    ret  = -M_PI*robotAlturas[typeFuzzySet::ESQUERDA];
    ret +=    0.0*robotAlturas[typeFuzzySet::FRENTE];
    ret +=  M_PI*robotAlturas[typeFuzzySet::DIREITA];
    if (div != 0) ret /= div; else ret /= 0.001;

    z = ret;
    f_value[typeFuzzySet::ESQUERDA] = calcPertRobot(typeFuzzySet::ESQUERDA, z);
    f_value[typeFuzzySet::FRENTE] = calcPertRobot(typeFuzzySet::FRENTE, z);
    f_value[typeFuzzySet::DIREITA] = calcPertRobot(typeFuzzySet::DIREITA, z);

    return ret;
    /*

    // Esta é a variável alfa1..alfa9, pois são 9 regras
    float regras[MAX_LENGTH_ROLES];

    regras[0] = std::min(f_bola[ESQUERDA], f_alvo[ESQUERDA]);
    regras[1] = std::min(f_bola[ESQUERDA], f_alvo[FRENTE]);
    regras[2] = std::min(f_bola[ESQUERDA], f_alvo[DIREITA]);
    regras[3] = std::min(f_bola[FRENTE],   f_alvo[ESQUERDA]);
    regras[4] = std::min(f_bola[FRENTE],   f_alvo[FRENTE]);
    regras[5] = std::min(f_bola[FRENTE],   f_alvo[DIREITA]);
    regras[6] = std::min(f_bola[DIREITA],  f_alvo[ESQUERDA]);
    regras[7] = std::min(f_bola[DIREITA],  f_alvo[FRENTE]);
    regras[8] = std::min(f_bola[DIREITA],  f_alvo[DIREITA]);


    // Calcula as alturas
    robotAlturas[ESQUERDA] = std::max(regras[1], std::max(regras[2], regras[5]));
    robotAlturas[FRENTE]   = std::max(regras[0], std::max(regras[4], regras[8]));
    robotAlturas[DIREITA]  = std::max(regras[3], std::max(regras[6], regras[7]));

    // Calcula o valor de saída
    float div = (robotAlturas[ESQUERDA] + robotAlturas[FRENTE] + robotAlturas[DIREITA]);

    ret  =  -M_PI * robotAlturas[ESQUERDA];
    ret +=    0.0 * robotAlturas[FRENTE];
    ret +=   M_PI * robotAlturas[DIREITA];
    if (div != 0) ret /= div; else ret /= 0.001;

    z = ret;
    f_value[ESQUERDA] = calcPertRobot(ESQUERDA, z);
    f_value[FRENTE]   = calcPertRobot(FRENTE,   z);
    f_value[DIREITA]  = calcPertRobot(DIREITA,  z);

    #if(TYPES_FUZZY_DEBUG)
    printf("TFuzzy::CalculaRegrasR(float xAlvo = %f, float xBola = %f) = %f\n", xAlvo, xBola, z);
    printf("f_value[ ESQUERDA FRENTE DIREITA ] = [ %f %f %f]\n", f_value[ESQUERDA], f_value[FRENTE], f_value[DIREITA]);
    #endif

    return z;

    */
}
