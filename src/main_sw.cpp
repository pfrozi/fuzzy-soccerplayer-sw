#include "../include/main_sw.h"

void help(){

    printf("\n");
    printf("\n# Trabalho de Redes Neurais e Fuzzy #");
    printf("\n   - Futebol de Robos - SW Player - ");
    printf("\n");
    printf("\n\t-h\t, Ajuda");
    printf("\n\t-s\t, IP do Servidor");
    printf("\n\t-p\t, Porta");
    printf("\n");

}


int main( int argc, char* argv[] ) {


    PlayerSW player;

    #if(MAIN_SW_DEBUG)
    printf("main_sw: main() init\n");
    #endif

    int    host_port = -1;
    char*  host;


    if (argc < ARGS_MAX || (argc==1 && !(strcmp(argv[1],ARGS_HELP)==0) )) {

        printf("Argumentos invalidos!\n");

        help();
        return (ERROR_INVALID_ARGS);

    } else {

        int i;

        for (i = 1; i < argc; i+=2) {

            if (i + 1 != argc)
            {
                char* arg = argv[i+1];

                if (strcmp(argv[i],ARGS_HELP) == 0) {

                    help();
                    return (RETURN_NOTHING);

                } else if (strcmp(argv[i], ARGS_PORT_NUMBER)==0) {

                    host_port = atoi(arg);

                } else if (strcmp(argv[i], ARGS_HOST_NUMBER)==0) {

                    host = arg;

                }else {

                    printf("Argumentos invalidos!\n");
                    return (ERROR_INVALID_ARGS);

                }
            }
        }
    }


    if(!(host_port>=0 && host_port<=65535)){

        printf("ERR: %s (port %d)\n", ERR_PORT_NUMBER, host_port);
        return(ERROR_INVALID_PORT);

    } else if(host_port>=0 && host_port<=1023){

        printf("ERR: %s (port %d)\n", ERR_PORT_RESERVED, host_port);
        return(ERROR_INVALID_PORT);

    }

    #if(MAIN_SW_DEBUG)
    printf("main_sw: main() - selected host %s\n", host);
    printf("main_sw: main() - selected host port %d\n", host_port);
    #endif


    player.SetHost(host, host_port);

    // Starting
    if(player.Play() > PLAY_OK){

        return (ERROR_SW);
    }

    return (RETURN_SUCCESS);



}
//------------------------------------------------------------------------------

