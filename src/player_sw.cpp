#include "../include/player_sw.hpp"

PlayerSW::PlayerSW(){

}
PlayerSW::~PlayerSW(){

}

void PlayerSW::SetHost(char* host, int port){

    host_ip = host;
    host_port = port;

    #if(PLAYER_SW_DEBUG)
    printf("PlayerSW::SetHost()\n");
    printf("\thost      = %s \n", host_ip);
    printf("\thost_port = %d \n", host_port);
    #endif

}

int  PlayerSW::Play(){

    long steps  = 0;
    long steps30= 0;
    float ant_f = 0.0f;

    float leftMotorAnt = 0.0f;
    float rightMotorAnt = 0.0f;

    float sLeftMotorAnt = 0.0f;
    float sRightMotorAnt = 0.0f;

    float stepf = 0.0f;
    #if(PLAYER_SW_DEBUG)
    printf("PlayerSW::Play()\tInit...\n");
    #endif

    // seta os parametros fuzzy

    fuzzy.SetConjAlvo(typeFuzzySet::ESQUERDA, -M_PI*1.001, -M_PI, 0);
    fuzzy.SetConjAlvo(typeFuzzySet::FRENTE, -M_PI_2, 0, M_PI_2);
    fuzzy.SetConjAlvo(typeFuzzySet::DIREITA, 0, M_PI, M_PI);

    fuzzy.SetConjBola(typeFuzzySet::ESQUERDA, -M_PI*1.001, -M_PI, 0);
    fuzzy.SetConjBola(typeFuzzySet::FRENTE, -M_PI_2, 0, M_PI_2);
    fuzzy.SetConjBola(typeFuzzySet::DIREITA, 0, M_PI, M_PI*1.001);

    fuzzy.SetConjRobot(typeFuzzySet::ESQUERDA, -M_PI*1.001, -M_PI, 0);
    fuzzy.SetConjRobot(typeFuzzySet::FRENTE, -M_PI, 0, M_PI);
    fuzzy.SetConjRobot(typeFuzzySet::DIREITA, 0, M_PI, M_PI*1.001);

    fuzzy.SetRegra1(typeFuzzySet::ESQUERDA, typeFuzzySet::ESQUERDA, typeFuzzySet::FRENTE);
    fuzzy.SetRegra1(typeFuzzySet::ESQUERDA, typeFuzzySet::FRENTE, typeFuzzySet::DIREITA);
    fuzzy.SetRegra1(typeFuzzySet::ESQUERDA, typeFuzzySet::DIREITA, typeFuzzySet::DIREITA);

    fuzzy.SetRegra1(typeFuzzySet::FRENTE, typeFuzzySet::ESQUERDA, typeFuzzySet::ESQUERDA);
    fuzzy.SetRegra1(typeFuzzySet::FRENTE, typeFuzzySet::FRENTE, typeFuzzySet::FRENTE);
    fuzzy.SetRegra1(typeFuzzySet::FRENTE, typeFuzzySet::DIREITA, typeFuzzySet::DIREITA);

    fuzzy.SetRegra1(typeFuzzySet::DIREITA, typeFuzzySet::ESQUERDA, typeFuzzySet::ESQUERDA);
    fuzzy.SetRegra1(typeFuzzySet::DIREITA, typeFuzzySet::FRENTE, typeFuzzySet::ESQUERDA);
    fuzzy.SetRegra1(typeFuzzySet::DIREITA, typeFuzzySet::DIREITA, typeFuzzySet::FRENTE);

    // Conecta-se ao SoccerMatch. Supõe que SoccerMatch está rodando na máquina
    // local e que um dos robôs esteja na porta 1024. Porta e local podem mudar.
    if ( ! environment.connect( host_ip, host_port) ) {
        printf( "\nFail connecting to the SoccerMatch.\n" );
        return PLAYER_SW_ERROR_CONN;  // Cancela operação se não conseguiu conectar-se.
    }

    #if(PLAYER_SW_DEBUG)
    printf("PlayerSW::Play()\tConnected!\n");
    #endif

    while ( 1 ) {

        #if(PLAYER_SW_DEBUG)
        printf("PlayerSW::Play()\tStep %d\n", steps++);
        #endif

        // Deve obter os dados desejados do ambiente. Métodos do clientEnvironm.
        // Exemplos de métodos que podem ser utilizados.
        ballAngle   = environment.getBallAngle();
        //targetAngle = environment.getTargetAngle( environment.getOwnGoal() );
        targetAngle = environment.getTargetAngle( environment.getOwnGoal() );
        ballDist    = environment.getDistance();

        bool isCollision = environment.getCollision()<10;

        float f = fuzzy.CalculaRegrasR(targetAngle, ballAngle);


        #if(PLAYER_SW_DEBUG)
        printf("PlayerSW::Play() Angulo Alvo = %f\n", targetAngle);
        printf("PlayerSW::Play() Angulo Bola = %f\n", ballAngle);
        printf("PlayerSW::Play() Distancia Bola = %f\n", ballDist);
        #endif

        #if(PLAYER_SW_DEBUG)
        printf("PlayerSW::Play() f = %f\n", f);
        printf("PlayerSW::Play() ESQUERDA = %f\n", fuzzy.f_value[typeFuzzySet::ESQUERDA]);
        printf("PlayerSW::Play() FRENTE = %f\n", fuzzy.f_value[typeFuzzySet::FRENTE]);
        printf("PlayerSW::Play() DIREITA = %f\n", fuzzy.f_value[typeFuzzySet::DIREITA]);
        #endif


        leftMotorAnt = leftMotor  ;
        rightMotorAnt = rightMotor ;

        leftMotor  = 1.0;
        rightMotor = 1.0;



        rightMotor *= (1 - fuzzy.f_value[typeFuzzySet::ESQUERDA]);
        leftMotor  *= (fuzzy.f_value[typeFuzzySet::ESQUERDA]);

        leftMotor   *= (1 - fuzzy.f_value[typeFuzzySet::DIREITA]);
        rightMotor  *= (fuzzy.f_value[typeFuzzySet::DIREITA]);

        if(fuzzy.f_value[typeFuzzySet::FRENTE]>=0.95f)
        {
            leftMotor  *= (1.2f+fuzzy.f_value[typeFuzzySet::FRENTE]);
            rightMotor *= (1.2f+fuzzy.f_value[typeFuzzySet::FRENTE]);

            stepf += 0.05f;

        }else if(fuzzy.f_value[typeFuzzySet::FRENTE]>=0.6f)
        {
            leftMotor  *= (1+fuzzy.f_value[typeFuzzySet::FRENTE]);
            rightMotor *= (1+fuzzy.f_value[typeFuzzySet::FRENTE]);
            stepf = 0.02f;

        }else if(fuzzy.f_value[typeFuzzySet::FRENTE]<0.6f)
        {
            leftMotor  *= (0.5+fuzzy.f_value[typeFuzzySet::FRENTE]);
            rightMotor *= (0.5+fuzzy.f_value[typeFuzzySet::FRENTE]);
            stepf = 0.0f;
        }

        leftMotor  +=stepf;
        rightMotor +=stepf;

        #if(PLAYER_SW_DEBUG)
        printf("PlayerSW::Play() MOTORES\n");
        printf("PlayerSW::Play() leftMotor = %f\n", leftMotor);
        printf("PlayerSW::Play() rightMotor = %f\n", rightMotor);
        #endif

        if(isCollision || (steps30>0 && steps30<30)) {
            float aux = leftMotor;

            leftMotor =-rightMotor;
            rightMotor =-aux;

            steps30++;
            if(steps30>30)  steps30 = 0;
        }

        // Transmite ação do robô ao ambiente. Fica bloqueado até que todos os
        // robôs joguem. Se erro, retorna false (neste exemplo, sai do laco).
        if ( ! environment.act( leftMotor, rightMotor ) ) {
            break; // Termina a execução se falha ao agir.
        }
    }

    return 0;
}
